package com.jiawa.wiki.service;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiawa.wiki.domain.Content;
import com.jiawa.wiki.domain.Doc;
import com.jiawa.wiki.domain.DocExample;
import com.jiawa.wiki.exception.BusinessException;
import com.jiawa.wiki.exception.BusinessExceptionCode;
import com.jiawa.wiki.mapper.ContentMapper;
import com.jiawa.wiki.mapper.DocMapper;
import com.jiawa.wiki.mapper.DocMapperCust;
import com.jiawa.wiki.req.DocQueryReq;
import com.jiawa.wiki.req.DocSaveReq;
import com.jiawa.wiki.resp.DocQueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.RedisUtil;
import com.jiawa.wiki.util.RequestContext;
import com.jiawa.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DocService {
	private static final Logger LOG = LoggerFactory.getLogger(DocService.class);

	@Resource
	private DocMapper DocMapper;

	@Resource
	private DocMapperCust docMapperCust;

	@Resource
	private SnowFlake snowFlake;

	@Resource
	public RedisUtil redisUtil;

	@Resource
	private ContentMapper contentMapper;

	@Resource
	public WsService wsService;

	public List<DocQueryResp> all(Long ebookId) {
		DocExample docExample = new DocExample();//这两行是固定的增加条件
		docExample.createCriteria().andEbookIdEqualTo(ebookId);
		docExample.setOrderByClause("sort asc");
		List<Doc> docList = DocMapper.selectByExample(docExample);

		List<DocQueryResp> respList = CopyUtil.copyList(docList, DocQueryResp.class);

		return  respList;
	}
	public PageResp<DocQueryResp> list(DocQueryReq req) {
		DocExample docExample = new DocExample();//这两行是固定的增加条件
		docExample.setOrderByClause("sort asc");
		DocExample.Criteria criteria = docExample.createCriteria();//相当于while条件
		if(!ObjectUtils.isEmpty(req.getName())){
			criteria.andNameLike("%" + req.getName() + "%");
		}
		PageHelper.startPage(req.getPage(),req.getSize());
		List<Doc> docList = DocMapper.selectByExample(docExample);

		PageInfo<Doc> pageInfo = new PageInfo<Doc>(docList);
		LOG.info("总页数：{}",pageInfo.getPages());
		LOG.info("总行数：{}",pageInfo.getTotal());

//		List<DocResp> respList = new ArrayList<>();
//		for (Doc doc : docList){
////			DocResp docResp = new DocResp();
////			BeanUtils.copyProperties(doc,docResp);
//
//			DocResp docResp = CopyUtil.copy(doc, DocResp.class);
//
////			docResp.setId(123L);测试
//			respList.add(docResp);
//		}

		List<DocQueryResp> respList = CopyUtil.copyList(docList, DocQueryResp.class);
		PageResp<DocQueryResp> pageResp = new PageResp<>();
		pageResp.setList(respList);
		pageResp.setTotal(pageInfo.getTotal());
		return  pageResp;
	}
	/*
	* 保存
	*
	*/
	@Transactional
	public void save(DocSaveReq req) {
		Doc doc = CopyUtil.copy(req, Doc.class);
		Content content = CopyUtil.copy(req, Content.class);
		if (ObjectUtils.isEmpty(doc.getId())) {
			//生成ID
			doc.setId(snowFlake.nextId());
			//新增
			doc.setViewCount(0);
			doc.setVoteCount(0);
			//因为insert方法会默认没有的就插null，可以看其sql语句
			DocMapper.insert(doc);

			content.setId(doc.getId());
			//新增
			contentMapper.insert(content);
		}else{
			//保存
			DocMapper.updateByPrimaryKey(doc);
			//保存
			int count = contentMapper.updateByPrimaryKeyWithBLOBs(content);
			if(count == 0){
				contentMapper.insert(content);
			}
	}
	}
	/*
	 * 删除
	 *
	 */
	public void delete(Long id) {
		DocMapper.deleteByPrimaryKey(id);
	}

	/*
	 *批量删除
	 *
	 */
	public void delete(List<String> ids) {
		DocExample docExample = new DocExample();//这两行是固定的增加条件
		DocExample.Criteria criteria = docExample.createCriteria();//相当于while条件
		criteria.andIdIn(ids);
		DocMapper.deleteByExample(docExample);
	}

	/*
	 * 查内容
	 *
	 */
	public String findContent(Long id) {
		Content content = contentMapper.selectByPrimaryKey(id);
		// 文档阅读数+1
		docMapperCust.increaseViewCount(id);
		if (ObjectUtils.isEmpty(content)) {
			return "";
		} else {
			return content.getContent();
		}
	}
	/**
	 * 点赞
	 */
	public void vote(Long id) {
		// docMapperCust.increaseVoteCount(id);
		// 远程IP+doc.id作为key，24小时内不能重复
		String ip = RequestContext.getRemoteAddr();
		if (redisUtil.validateRepeat("DOC_VOTE_" + id + "_" + ip, 5000)) {
			docMapperCust.increaseVoteCount(id);
		} else {
			throw new BusinessException(BusinessExceptionCode.VOTE_REPEAT);
		}

		// 推送消息
		Doc docDb = DocMapper.selectByPrimaryKey(id);
		String logId = MDC.get("LOG_ID");//获取当前线程流水号
		wsService.sendInfo("【" + docDb.getName() + "】被点赞！", logId);
//		 rocketMQTemplate.convertAndSend("VOTE_TOPIC", "【" + docDb.getName() + "】被点赞！");
	}

	public void updateEbookInfo() {
		docMapperCust.updateEbookInfo();
	}
}
