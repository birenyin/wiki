package com.jiawa.wiki.service;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiawa.wiki.domain.Category;
import com.jiawa.wiki.domain.CategoryExample;
import com.jiawa.wiki.mapper.CategoryMapper;
import com.jiawa.wiki.req.CategoryQueryReq;
import com.jiawa.wiki.req.CategorySaveReq;
import com.jiawa.wiki.resp.CategoryQueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryService {
	private static final Logger LOG = LoggerFactory.getLogger(CategoryService.class);

	@Resource
	private CategoryMapper CategoryMapper;

	@Resource
	private SnowFlake snowFlake;

	public List<CategoryQueryResp> all() {
		CategoryExample categoryExample = new CategoryExample();//这两行是固定的增加条件
		categoryExample.setOrderByClause("sort asc");
		List<Category> categoryList = CategoryMapper.selectByExample(categoryExample);

		List<CategoryQueryResp> respList = CopyUtil.copyList(categoryList, CategoryQueryResp.class);

		return  respList;
	}
	public PageResp<CategoryQueryResp> list(CategoryQueryReq req) {
		CategoryExample categoryExample = new CategoryExample();//这两行是固定的增加条件
		categoryExample.setOrderByClause("sort asc");
		CategoryExample.Criteria criteria = categoryExample.createCriteria();//相当于while条件
		if(!ObjectUtils.isEmpty(req.getName())){
			criteria.andNameLike("%" + req.getName() + "%");
		}
		PageHelper.startPage(req.getPage(),req.getSize());
		List<Category> categoryList = CategoryMapper.selectByExample(categoryExample);

		PageInfo<Category> pageInfo = new PageInfo<Category>(categoryList);
		LOG.info("总页数：{}",pageInfo.getPages());
		LOG.info("总行数：{}",pageInfo.getTotal());

//		List<CategoryResp> respList = new ArrayList<>();
//		for (Category category : categoryList){
////			CategoryResp categoryResp = new CategoryResp();
////			BeanUtils.copyProperties(category,categoryResp);
//
//			CategoryResp categoryResp = CopyUtil.copy(category, CategoryResp.class);
//
////			categoryResp.setId(123L);测试
//			respList.add(categoryResp);
//		}

		List<CategoryQueryResp> respList = CopyUtil.copyList(categoryList, CategoryQueryResp.class);
		PageResp<CategoryQueryResp> pageResp = new PageResp<>();
		pageResp.setList(respList);
		pageResp.setTotal(pageInfo.getTotal());
		return  pageResp;
	}
	/*
	* 保存
	*
	*/
	public void save(CategorySaveReq req) {
		Category category = CopyUtil.copy(req, Category.class);
		if (ObjectUtils.isEmpty(category.getId())) {
			//生成ID
			category.setId(snowFlake.nextId());
			//新增
			CategoryMapper.insert(category);
		}else{
			//保存
			CategoryMapper.updateByPrimaryKey(category);
	}
	}
	/*
	 * 删除
	 *
	 */
	public void delete(Long id) {
		CategoryMapper.deleteByPrimaryKey(id);
	}
}
