package com.jiawa.wiki.service;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiawa.wiki.domain.User;
import com.jiawa.wiki.domain.UserExample;
import com.jiawa.wiki.exception.BusinessException;
import com.jiawa.wiki.exception.BusinessExceptionCode;
import com.jiawa.wiki.mapper.UserMapper;
import com.jiawa.wiki.req.UserLoginReq;
import com.jiawa.wiki.req.UserQueryReq;
import com.jiawa.wiki.req.UserResetPasswordReq;
import com.jiawa.wiki.req.UserSaveReq;
import com.jiawa.wiki.resp.UserLoginResp;
import com.jiawa.wiki.resp.UserQueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

	@Resource
	private UserMapper UserMapper;

	@Resource
	private SnowFlake snowFlake;

	public PageResp<UserQueryResp> list(UserQueryReq req) {
		UserExample userExample = new UserExample();//这两行是固定的增加条件
		UserExample.Criteria criteria = userExample.createCriteria();//相当于while条件
		if(!ObjectUtils.isEmpty(req.getLoginName())){
			criteria.andNameLike("%" + req.getLoginName() + "%");
		}
		PageHelper.startPage(req.getPage(),req.getSize());
		List<User> userList = UserMapper.selectByExample(userExample);

		PageInfo<User> pageInfo = new PageInfo<User>(userList);
		LOG.info("总页数：{}",pageInfo.getPages());
		LOG.info("总行数：{}",pageInfo.getTotal());

//		List<UserResp> respList = new ArrayList<>();
//		for (User user : userList){
////			UserResp userResp = new UserResp();
////			BeanUtils.copyProperties(user,userResp);
//
//			UserResp userResp = CopyUtil.copy(user, UserResp.class);
//
////			userResp.setId(123L);测试
//			respList.add(userResp);
//		}

		List<UserQueryResp> respList = CopyUtil.copyList(userList, UserQueryResp.class);
		PageResp<UserQueryResp> pageResp = new PageResp<>();
		pageResp.setList(respList);
		pageResp.setTotal(pageInfo.getTotal());
		return  pageResp;
	}

	/**
	 * 保存
	 */
	public void save(UserSaveReq req) {
		User user = CopyUtil.copy(req, User.class);
		if (ObjectUtils.isEmpty(req.getId())) {
			User userDB = selectByLoginName(req.getLoginName());
			if (ObjectUtils.isEmpty(userDB)) {
				// 新增
				user.setId(snowFlake.nextId());
				UserMapper.insert(user);
			} else {
				// 用户名已存在
				throw new BusinessException(BusinessExceptionCode.USER_LOGIN_NAME_EXIST);
			}
		} else {
			// 更新
			user.setLoginName(null);
			user.setPassword(null);
			UserMapper.updateByPrimaryKeySelective(user);
		}
	}

	public void delete(Long id) {
		UserMapper.deleteByPrimaryKey(id);
	}

	//查找用户登入名
	public User selectByLoginName(String LoginName) {
		UserExample userExample = new UserExample();
		UserExample.Criteria criteria = userExample.createCriteria();
		criteria.andLoginNameEqualTo(LoginName);
		List<User> userList = UserMapper.selectByExample(userExample);
		if (CollectionUtils.isEmpty(userList)) {
			return null;
		} else {
			return userList.get(0);
		}
	}

	/**
	 * 修改密码
	 */
	public void resetPassword(UserResetPasswordReq req) {
		User user = CopyUtil.copy(req, User.class);
		UserMapper.updateByPrimaryKeySelective(user);
	}


	/**
	 * 登录
	 */
	public UserLoginResp login(UserLoginReq req) {
		User userDb = selectByLoginName(req.getLoginName());
		if (ObjectUtils.isEmpty(userDb)) {
			// 用户名不存在
			LOG.info("用户名不存在, {}", req.getLoginName());
			throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
		} else {
			if (userDb.getPassword().equals(req.getPassword())) {
				// 登录成功
				UserLoginResp userLoginResp = CopyUtil.copy(userDb, UserLoginResp.class);
				return userLoginResp;
			} else {
				// 密码不对
				LOG.info("密码不对, 输入密码：{}, 数据库密码：{}", req.getPassword(), userDb.getPassword());
				throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
			}
		}
	}

}
