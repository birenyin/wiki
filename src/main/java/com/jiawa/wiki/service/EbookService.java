package com.jiawa.wiki.service;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiawa.wiki.domain.Ebook;
import com.jiawa.wiki.domain.EbookExample;
import com.jiawa.wiki.mapper.EbookMapper;
import com.jiawa.wiki.req.EbookQueryReq;
import com.jiawa.wiki.req.EbookSaveReq;
import com.jiawa.wiki.resp.EbookQueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EbookService {
	private static final Logger LOG = LoggerFactory.getLogger(EbookService.class);

	@Resource
	private EbookMapper EbookMapper;

	@Resource
	private SnowFlake snowFlake;

	public PageResp<EbookQueryResp> list(EbookQueryReq req) {
		EbookExample ebookExample = new EbookExample();//这两行是固定的增加条件
		EbookExample.Criteria criteria = ebookExample.createCriteria();//相当于while条件
		if(!ObjectUtils.isEmpty(req.getName())){
			criteria.andNameLike("%" + req.getName() + "%");
		}
		if(!ObjectUtils.isEmpty(req.getCategoryId2())){
			criteria.andCategory2IdEqualTo(req.getCategoryId2());
		}
		PageHelper.startPage(req.getPage(),req.getSize());
		List<Ebook> ebookList = EbookMapper.selectByExample(ebookExample);

		PageInfo<Ebook> pageInfo = new PageInfo<Ebook>(ebookList);
		LOG.info("总页数：{}",pageInfo.getPages());
		LOG.info("总行数：{}",pageInfo.getTotal());

//		List<EbookResp> respList = new ArrayList<>();
//		for (Ebook ebook : ebookList){
////			EbookResp ebookResp = new EbookResp();
////			BeanUtils.copyProperties(ebook,ebookResp);
//
//			EbookResp ebookResp = CopyUtil.copy(ebook, EbookResp.class);
//
////			ebookResp.setId(123L);测试
//			respList.add(ebookResp);
//		}

		List<EbookQueryResp> respList = CopyUtil.copyList(ebookList, EbookQueryResp.class);
		PageResp<EbookQueryResp> pageResp = new PageResp<>();
		pageResp.setList(respList);
		pageResp.setTotal(pageInfo.getTotal());
		return  pageResp;
	}
	/*
	* 保存
	*
	*/
	public void save(EbookSaveReq req) {
		Ebook ebook = CopyUtil.copy(req, Ebook.class);
		if (ObjectUtils.isEmpty(ebook.getId())) {
			//生成ID
			ebook.setId(snowFlake.nextId());
			//新增
			EbookMapper.insert(ebook);
		}else{
			LOG.info("封面：{}",ebook.getCover());
			LOG.info("ID：{}",ebook.getId());
			//保存
			EbookMapper.updateByPrimaryKey(ebook);
	}
	}
	/*
	 * 删除
	 *
	 */
	public void delete(Long id) {
		EbookMapper.deleteByPrimaryKey(id);
	}
}
