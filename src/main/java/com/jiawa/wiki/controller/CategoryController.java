package com.jiawa.wiki.controller;


import com.jiawa.wiki.req.CategoryQueryReq;
import com.jiawa.wiki.req.CategorySaveReq;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.resp.CategoryQueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.service.CategoryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
	@Resource
	private CategoryService categoryservice;

	@GetMapping("/all")
	public CommonResp all() {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<List<CategoryQueryResp>> resp = new CommonResp<>();
		List<CategoryQueryResp> list = categoryservice.all();
		resp.setContent(list);
		return resp;
	}
	@GetMapping("/list")
	public CommonResp list(@Valid CategoryQueryReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<PageResp<CategoryQueryResp>> resp = new CommonResp<>();
		PageResp<CategoryQueryResp> list = categoryservice.list(req);
		resp.setContent(list);
		return resp;
	}
	@PostMapping("/save")
	public CommonResp save(@Valid @RequestBody CategorySaveReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		categoryservice.save(req);
		return resp;
	}
	@DeleteMapping("/delete/{id}")
	public CommonResp delete(@PathVariable Long id) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		categoryservice.delete(id);
		return resp;
	}
}
