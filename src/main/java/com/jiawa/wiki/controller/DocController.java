package com.jiawa.wiki.controller;


import com.jiawa.wiki.req.DocQueryReq;
import com.jiawa.wiki.req.DocSaveReq;
import com.jiawa.wiki.resp.DocQueryResp;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.service.DocService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/doc")
public class DocController {
	@Resource
	private DocService docservice;

	@GetMapping("/all/{ebookId}")
	public CommonResp list(@PathVariable Long ebookId) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<List<DocQueryResp>> resp = new CommonResp<>();
		List<DocQueryResp> list = docservice.all(ebookId);
		resp.setContent(list);
		return resp;
	}
	@GetMapping("/list")
	public CommonResp list(@Valid DocQueryReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<PageResp<DocQueryResp>> resp = new CommonResp<>();
		PageResp<DocQueryResp> list = docservice.list(req);
		resp.setContent(list);
		return resp;
	}
	@PostMapping("/save")
	public CommonResp save(@Valid @RequestBody DocSaveReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		docservice.save(req);
		return resp;
	}
	@DeleteMapping("/delete/{idsStr}")
	public CommonResp delete(@PathVariable String idsStr) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		List<String> list = Arrays.asList(idsStr.split(","));
		docservice.delete(list);
		return resp;
	}
	@GetMapping("/find-content/{id}")
	public CommonResp findContent(@PathVariable Long id) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<String> resp = new CommonResp<>();
		String content = docservice.findContent(id);
		resp.setContent(content);
		return resp;
	}
	@GetMapping("/vote/{id}")
	public CommonResp vote(@PathVariable Long id) {
		CommonResp commonResp = new CommonResp();
		docservice.vote(id);
		return commonResp;
	}
}

