package com.jiawa.wiki.controller;


import com.jiawa.wiki.req.EbookQueryReq;
import com.jiawa.wiki.req.EbookSaveReq;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.resp.EbookQueryResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.service.EbookService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/ebook")
public class EbookController {
	@Resource
	private EbookService ebookservice;

	@GetMapping("/list")
	public CommonResp list(@Valid EbookQueryReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<PageResp<EbookQueryResp>> resp = new CommonResp<>();
		PageResp<EbookQueryResp> list = ebookservice.list(req);
		resp.setContent(list);
		return resp;
	}
	@PostMapping("/save")
	public CommonResp save(@Valid @RequestBody EbookSaveReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		ebookservice.save(req);
		return resp;
	}
	@DeleteMapping("/delete/{id}")
	public CommonResp delete(@PathVariable Long id) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		ebookservice.delete(id);
		return resp;
	}
}
