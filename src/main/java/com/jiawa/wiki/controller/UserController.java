package com.jiawa.wiki.controller;


import com.alibaba.fastjson.JSONObject;
import com.jiawa.wiki.req.UserLoginReq;
import com.jiawa.wiki.req.UserQueryReq;
import com.jiawa.wiki.req.UserResetPasswordReq;
import com.jiawa.wiki.req.UserSaveReq;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.resp.PageResp;
import com.jiawa.wiki.resp.UserLoginResp;
import com.jiawa.wiki.resp.UserQueryResp;
import com.jiawa.wiki.service.UserService;
import com.jiawa.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/user")
public class UserController {
	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	@Resource
	private UserService userservice;

	@Resource
	private SnowFlake snowFlake;

	@Resource
	private RedisTemplate redisTemplate;
	@GetMapping("/list")
	public CommonResp list(@Valid UserQueryReq req) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp<PageResp<UserQueryResp>> resp = new CommonResp<>();
		PageResp<UserQueryResp> list = userservice.list(req);
		resp.setContent(list);
		return resp;
	}
	@PostMapping("/save")
	public CommonResp save(@Valid @RequestBody UserSaveReq req) {//参数名字一样会自动映射,类属性也会自动映射
		req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
		CommonResp resp = new CommonResp<>();
		userservice.save(req);
		return resp;
	}
	@DeleteMapping("/delete/{id}")
	public CommonResp delete(@PathVariable Long id) {//参数名字一样会自动映射,类属性也会自动映射
		CommonResp resp = new CommonResp<>();
		userservice.delete(id);
		return resp;
	}

	@PostMapping("/reset-password")
	public CommonResp resetPassword(@Valid @RequestBody UserResetPasswordReq req) {
		req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
		CommonResp resp = new CommonResp<>();
		userservice.resetPassword(req);
		return resp;
	}

	@PostMapping("/login")
	public CommonResp login(@Valid @RequestBody UserLoginReq req) {
		req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
		CommonResp<UserLoginResp> resp = new CommonResp<>();
		UserLoginResp userLoginResp = userservice.login(req);

		Long token = snowFlake.nextId();
		LOG.info("生成单点登录token：{}，并放入redis中", token);
		userLoginResp.setToken(token.toString());
		//TimeUnit是java.util.concurrent包下面的一个类，表示给定单元粒度的时间段
		redisTemplate.opsForValue().set(token.toString(), JSONObject.toJSONString(userLoginResp), 3600 * 24, TimeUnit.SECONDS);

		resp.setContent(userLoginResp);
		return resp;
	}

	@GetMapping("/logout/{token}")
	public CommonResp logout(@PathVariable String token) {
		CommonResp resp = new CommonResp<>();
		redisTemplate.delete(token);
		LOG.info("从redis中删除token: {}", token);
		return resp;
	}

}
