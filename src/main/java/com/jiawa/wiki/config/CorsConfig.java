package com.jiawa.wiki.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")//映射的请求地址，这样写就是争对所有的请求地址
                .allowedOriginPatterns("*")//允许来源所有
                .allowedHeaders(CorsConfiguration.ALL)//所有请求头
                .allowedMethods(CorsConfiguration.ALL)//所有请求方法
                .allowCredentials(true)//允许前端请求带上自己的凭证，如cookie信息，session
                .maxAge(3600); // 1小时内不需要再预检（发OPTIONS请求）在调用接口时，前端会发一次预检
    }

}
